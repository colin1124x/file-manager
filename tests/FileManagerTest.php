<?php

class FileManagerTest extends FileManagerTestCase
{
    public function testSave()
    {
        $file = $this->fm->save($this->stub);

        $this->assertFileUploaded($file, '檢查檔案上傳機制是否正常');
    }

    public function testFetch()
    {
        $file_1 = $this->fm->save($this->stub);
        $file_2 = $this->fm->save($this->stub);

        $files = $this->fm->fetch(10);

        $this->assertEquals($file_1, $files[0]);
        $this->assertEquals($file_2, $files[1]);
    }

    public function testSearch()
    {
        $exacts = array();
        $test_cnt = 10;
        while (0 < $test_cnt--) {
            $file = $this->fm->save($this->stub);
            $exacts[$file['id']] = $file;
        }

        $results = $this->fm->search($ids = array_rand($exacts, 3));

        foreach ($ids as $id) {
            $this->assertArrayHasKey($id, $results);
            $this->assertEquals($exacts[$id], $results[$id]);
        }
    }

    public function testPurge()
    {
        $file = $this->fm->save($this->stub);

        $this->fm->purge($file['id']);

        // 測試容錯
        $this->fm->purge(100);

        $this->assertFileDeleted($file);
    }
}
