<?php

class FileManagerTestCase extends Orchestra\Testbench\TestCase
{
    protected $dir;
    protected $dir_uri = '/_files';
    protected $stub;

    /** @var \Colin\FileManager\Manager */
    protected $fm;

    public function setUp()
    {
        parent::setUp();

        $artisan = $this->app['artisan'];
        $artisan->call('migrate', array(
            '--database' => 'test',
            '--path' => '../src/migrations',
        ));

        $tmp = sys_get_temp_dir();
        $dir = $tmp.DIRECTORY_SEPARATOR.substr(sha1(microtime()), 0, 7);
        mkdir($dir);
        $this->dir = $dir;

        $stub = new Rde\Prototype();

        $stub->extend('guessExtension', function(){return 'jpg';})
            ->extend('move', function($self, $dir, $filename){
                fclose(fopen($f = "{$dir}/$filename", 'a'));

                return is_file($f);
            });

        $this->stub = $stub;

        $this->fm = $this->app->make('file.manager', array('dir' => $dir, 'dir_uri' => $this->dir_uri));
    }

    protected function tearDown()
    {
        if (is_dir($this->dir)) {
            $d = opendir($this->dir);
            while ($filename = readdir($d)) {
                ! preg_match('/^[.]{1,2}$/', $filename) and unlink($this->dir.DIRECTORY_SEPARATOR.$filename);
            }
            closedir($d);
            $this->assertTrue(rmdir($this->dir));
        }
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['path.base'] = __DIR__ . '/../src';
        $app['config']->set('database.default', 'test');
        $app['config']->set('database.connections.test', array(
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ));

        $app->register(new \Colin\FileManager\FileManagerServiceProvider($app));
    }

    protected function assertFileUploaded($file, $msg = null)
    {
        $this->assertArrayHasKey('id', $file, $msg);
        $this->assertArrayHasKey('name', $file, $msg);
        $this->assertArrayHasKey('extension', $file, $msg);
        $this->assertArrayHasKey('source', $file, $msg);

        $filename = "{$file['name']}.{$file['extension']}";
        $this->assertTrue(is_file("{$this->dir}/{$filename}"), $msg);
        $this->assertEquals(asset("{$this->dir_uri}/{$filename}"), $file['source'], $msg);
    }

    protected function assertFileDeleted($file, $msg = null)
    {
        $filename = "{$file['name']}.{$file['extension']}";
        $this->assertFalse(is_file("{$this->dir}/{$filename}"), $msg);
        $this->assertNull(\UploadedFile::find($file['id']));
    }
}
