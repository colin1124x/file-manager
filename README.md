# colin/file-manager
檔案上傳管理 - depends on [laravel 4.1.*], [symfony/filesystem]

---

## 直接建構 Manager 物件

```php

use Colin\FileManager\Manager;
$manager = new Manager($app, $dir, $dir_uri);

```

## 從 Ioc 取得

```php

$manager = App::make('file.manager', ['dir' => $dir, 'dir_uri' => $dir_uri]);

```

## Methods

```php

// 取得指定範圍檔案
$manager->fetch($limit [, $offset]);// array

// 查找指定 id(s) 圖片, 可以是陣列
$manager->search($id);// array

/** 
* 儲存圖片
* $file Symfony\Component\HttpFoundation\File\UploadedFile
*/
$manager->save($file [, $pre_save_handler [, $post_save_handler]]);

// 清除圖片
$manager->purge($id [, $pre_purge_handler [, $post_purge_handler]]);

```
[laravel 4.1.*]:http://laravel.tw/docs/4.2
[symfony/filesystem]:http://symfony.com/doc/current/components/filesystem/introduction.html