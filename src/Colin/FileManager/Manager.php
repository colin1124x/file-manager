<?php namespace Colin\FileManager;

use UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

class Manager
{
    private $app;
    private $dir;
    private $dir_uri;

    public function __construct($app, $dir, $dir_uri)
    {
        $this->app = $app;
        $this->dir = $dir;
        $this->dir_uri = $dir_uri;
    }

    public function fetch($limit, $offset = 0)
    {
        $limit = (int) $limit;
        $offset = (int) $offset;

        $ret = array();
        foreach (UploadedFile::limit($limit)->offset($offset)->get() as $file) {
            $ret[] = $this->resolveFileSrc($file);
        }

        return $ret;
    }

    public function search($id)
    {
        $id = array_map(function($n){ return (int) $n;}, (array) $id);
        $ret = array();

        if ( ! empty($id)) {
            foreach (UploadedFile::whereIn('id', $id)->get() as $upload_file) {
                $ret[$upload_file['id']] = $this->resolveFileSrc($upload_file);
            }
        }

        return $ret;
    }

    public function save($file, \Closure $pre_save = null, \Closure $post_save = null)
    {
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
        if ( ! $file || ! is_object($file)) return false;
        if ($pre_save && false === $pre_save($file)) return false;

        $name = sha1(microtime().rand(1, 999));
        $extension = $file->guessExtension();
        $full_name = "{$name}.{$extension}";

        $upload_file = new UploadedFile(array(
            'name' => $name,
            'extension' => $extension,
        ));

        $file->move($this->dir, $full_name);
        $upload_file->save();

        $result = $this->resolveFileSrc($upload_file);

        $post_save && $post_save($file, $result);

        return $result;
    }

    public function purge($id, \Closure $pre_purge = null, \Closure $post_purge = null)
    {
        $upload_file = UploadedFile::find($id);
        if ( ! $upload_file) return null;
        if ($pre_purge && false === $pre_purge($upload_file)) return false;

        $full_name = "{$this->dir}/{$upload_file['name']}.{$upload_file['extension']}";
        $this->app['files']->delete($full_name);
        $upload_file->delete();
        $post_purge and $post_purge($upload_file, $full_name);

        return $full_name;
    }

    protected function resolveFileSrc($file)
    {
        $file = $file->toArray();
        $file['source'] = asset("{$this->dir_uri}/{$file['name']}.{$file['extension']}");
        return $file;
    }
}
